%w[
.
./lib
].each do |dep|
  $LOAD_PATH.unshift(File.dirname(__FILE__) + "/#{dep}")
end

require 'ox_store'

if __FILE__ == $0
  def bootstrap

    p OxStore.version

    p "relation universe <> location"

    universe = OxUniverse.new('storage/universe.store', {:force => true})
    universe2 = OxUniverse.new('storage/universe2.store', {:force => true})
    location  = OxLocation.new('storage/location.store', {:force => true})
    location1  = OxLocation.new('storage/location1.store', {:force => true})
    location2  = OxLocation.new('storage/location2.store', {:force => true})
    loc = universe.adopt(location)

    loc1 = universe.adopt(location1)
    loc2 = universe.adopt(universe2, :universe)

    uni2 = universe.adopt(location2)

    family100 = OxFamily.new('family100', location, {:force => true})
    location.adopt(family100)

    location.reject(family100)
    location.adopt(family100)

    family100.related(family100)
    p family100.relation

    #universe.house.update(loc, {:location => 'pippo'})
    #universe.house.update({:location => 'pippo'}, {:location => 'storage/location.store'})

    universe.reject(location)
    loc = universe.adopt('storage/location.store')

    p "relation location <> family"

    family = OxFamily.new('family1', location, {:force => true})

    location.adopt(family)
    location.reject(family)
    location.adopt(family, orphans = true)
    #location.reject(family, {:kill => true})

    p "relation Family <> member"

    member1 = OxMember.new('member1', location)
    p member1.name
    p member1.family

    #p member1.family
    family.adopt(member1)

    member1.related(member1)
    p member1.relation

    #p member1.family
    family.reject(member1)
    #p member1.family
    family.adopt(member1)
    #p member1.family
    #family.reject(member1, {:kill => true})
    #p member1.family

    p "RENAME"
    p family.rename('member1', 'member1new')
    p member1.name
    p member1.family

    member2 = OxMember.new('member2', location)
    family.adopt(member2)

    p member2
    p member1
    member1.relink(member2)

    p member1
    member1.relink('member2')
    p member1
    member1.relink('member1')
    p member1

    member3 = family.instance(member2)
    member4 = family.instance('member2')
    p member3
    p member4

    p "member features interaction"

    member1.define('nome1', 'massimo', OxStore::MIME_TEXT_FEATURE_PLAIN)
    member1.define('nome2', 'massimo', OxStore::MIME_TEXT_FEATURE_PLAIN + OxStore::MIME_FEATURE_ENCRYPTED)
    member1.define('nome3', 'massimo', OxStore::MIME_TEXT_FEATURE_PLAIN + OxStore::MIME_FEATURE_COMPRESSED)
    member1.define('nome4', 'massimo', OxStore::MIME_TEXT_FEATURE_PLAIN + OxStore::MIME_FEATURE_ENCRYPTED_COMPRESSED)

    member1.define('code_plain.rb', %(

      class Pippo
        def hi
        "ciao, io sono Pippo"
        end
      end

      persona = Pippo.new
      p persona.hi

    ).strip, OxStore::MIME_CODE_FEATURE_RUBY)

    member1.read('code_plain.rb')

    member1.define('code2.rb', %(

      class Pippo
        def hi
        "ciao, io sono Pippo Cryptato"
        end
      end

    def hi2
      persona = Pippo.new
      p persona.hi
    end

    ), OxStore::MIME_CODE_FEATURE_RUBY + OxStore::MIME_FEATURE_ENCRYPTED)

    member1.read('code2.rb')

    aa = member1.hi2
    hi2

    xx = Pippo.new
    xx.hi2

    member1.define('pixmap', 'file://./pix.png', OxStore::MIME_BINARY_FEATURE_PNG)
    member1.define('remote_html', 'http://www.pragmas.org/index.html', OxStore::MIME_TEXT_FEATURE_HTML)

    p member1.read('nome1')
    p member1.read('nome2')
    p member1.read('nome3')
    p member1.read('nome4')

    member1.define('nome1', 'eleonora')
    p member1.read('nome1')

    member1.delete('nome1')
    p member1.read('nome1')

    member1.define('nome1', 'eleonora')
    p member1.read('nome1')

    p member1.datasets
    p member1.datasets(['feature','mimetype'], 1)
    p member1.datasets(['feature'], 2)
    p member1.datasets(:all)
    p member1.datasets(:all, :desc)
    p member1.datasets(:all, :desc, 2)
    p member1.datasets(2, :all, :desc)

    p "family some actions"

    family2 = OxFamily.new('family2', location, {:force => true})
    family2.import(family.datasets)

    p"CLONING FACILITY"
    p family.clone(member1, 'cloned_1_member1')
    p family.clone(member1, 'cloned_2_member1_light', :light)

    p family2
    p location.clone(family, 'cloned_2_family')
    p location.clone(family, 'cloned_2_family_light', :light)

    p location.name

    universe.metadefine("meta-name", "universe")
    location.metadefine("meta-name", "location")
    family.metadefine("meta-name", "family")
    member1.metadefine("meta-name", "member1")

    p ""
    p "datasets mimetype"
    universe  = OxUniverse.new('storage/universe_dsets.store', {:force => true})
    location  = OxLocation.new('storage/location_dsets.store', {:force => true})

    table = %(
    {
    "table" : {
      "name" : "products",
      "columns" : [
        {"name" : "id", "type" : "integer", "pkey" : true},
        {"name" : "name", "type" : "text", "indexed" : true},
        {"name" : "description", "type" : "text"}
      ]
    }
    }
    )

    location.house.prepare(table)
    location.house.inserts([{:name => 'sapone', :description => "l'iquido"}, {:name => 'sapone', :description => 'polvere'}], 'products')

    products_store = OxPhysicalStore.start("storage/products_store.store")
    products_store.prepare(table)
    products_store.inserts([{:name => 'sapone', :description => "l'iquido"}, {:name => 'sapone', :description => 'polvere'}, {:name => 'sapone', :description => 'solido'}], 'products')

    fam1 = location.create('family1')
    mbr1 = fam1.create('member1')

    mbr1.define('soaps0', {:collection => 'products', :location => "storage/products_store.store", :filter => {:name => 'sapone'}}, OxStore::MIME_DATASETS_FEATURE_FILTER)

    mbr1.define('soaps1', {:collection => 'products', :filter => {:name => 'sapone'}}, OxStore::MIME_DATASETS_FEATURE_FILTER)
    mbr1.define('soaps2', {:collection => 'products', :results => [:name, :description], :filter => {:name => 'sapone'}}, OxStore::MIME_DATASETS_FEATURE_FILTER)
    mbr1.define('soaps3', "select * from products", OxStore::MIME_DATASETS_FEATURE_SCRIPT)

    datasets0 = mbr1.read('soaps0')
    datasets1 = mbr1.read('soaps1')
    datasets2 = mbr1.read('soaps2')
    datasets3 = mbr1.read('soaps3')

    mbr1.define('soaps4', datasets3, OxStore::MIME_DATASETS_FEATURE_STORED)
    mbr1.define('soaps4', datasets3, OxStore::MIME_DATASETS_FEATURE_STORED)
    datasets4 = mbr1.read('soaps4')

    p 'datasets0'
    p datasets0
    p 'datasets1'
    p datasets1
    p 'datasets2'
    p datasets2
    p 'datasets3'
    p datasets3
    p 'datasets4'
    p datasets4

  end

  bootstrap
end