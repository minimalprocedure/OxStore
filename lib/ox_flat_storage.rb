# encoding: utf-8
# Copyright 2009-07-01, Massimo Maria Ghisalberti, Pragmas snc http://pragmas.org. All Rights Reserved.
# This is free software.
# licence: BSD 2-clause license ("Simplified BSD License" or "FreeBSD License") <http://www.freebsd.org/copyright/copyright.html>

# Ideas and basic implementations
# Massimo Maria Ghisalberti <massimo.ghisalberti@gmail.com, zairik@gmail.com, minimal.procedure@gmail.com >

# OxStore is a database key / value modeled on the metaphor of the human family 
# relationships and spatial distribution.

# Features implemented or being implemented.
# Logical units are: Universe, Location, Home, Family, Members (of the family).
# The units are logical containers of underlying units and control relationships.
# Family members may be born, adopted, orphanized, rejected or killed.
# Each logical relationship can have an unlimited number of properties of different types.
#...

# STATE: INITIAL;PLANNING --

require 'rubygems'
require 'rbconfig'

if Config::CONFIG['ruby_version'].match('1.8')
  require 'json'
elsif  Config::CONFIG['ruby_version'].match('1.9')
  require 'json/pure'
end

class OxFlatStorage

  private
  def table_exist?(table_name)
    schema = @schema.read.split("\n")
    schema.each {|s|
      data = JSON::load(s)
      return true if data['table'] && data['table'] == table_name
    }
    false
  end

  public

  def self.open(datastore)
    ox_flat_storage = OxFlatStorage.new(datastore)
  end

  def initialize(datastore)
    open(datastore)
  end

  def open(datastore = nil)
    @datastore = datastore
    @schema = File.join(@datastore, 'schema.json')
    unless File.exist?(@datastore)
      FileUtils.mkdir_p(@datastore)
      @schema = File.open(@schema, 'a+')
      initial = {:datastore => @datastore, :schema => '', :version => '1.0.0.0'}
      @schema << initial.to_json
      @schema << "\n"
      @schema.flush
    else
      @schema = File.open(@schema, 'a+')
    end
  end

  def close
    @schema.close
    @schema = nil
    @datastore = nil
  end

  def execute(script)
  end

  def execute2(script)
  end

  def execute_batch(script)
    data = JSON::load(script)
    unless table_exist?(data['table']['name'])
      @schema << {:table => data['table']['name']}.to_json
      @schema << "\n"
      table = File.join(@datastore, "#{data['table']['name']}.json")
      File.open(table, 'a'){|t| t << data.to_json; t << "\n"}
    end
  end

end

if __FILE__ == $0
  def bootstrap
    store = OxFlatStorage.open('storage.store')
    store.execute_batch(
    %(
        {
        "table" : {
          "name" : "universe",
          "columns" : [
            {"name" : "id", "type" : "integer", "pkey" : true},
            {"name" : "location", "type" : "text", "unique" : true, "indexed" : true},
            {"name" : "mimetype", "type" : "text", "not_null" : true, "indexed" : true},
            {"name" : "storetype", "type" : "text", "default" : "sqlite", "indexed" : true},
            {"name" : "hash", "type" : "text", "default" : null, "indexed" : true},
            {"name" : "created", "type" : "time"}
          ]
        }
        }
     )
    )
    store.execute_batch(
    %(
            {
            "table" : {
              "name" : "tabella2",
              "columns" : [
                {"name" : "id", "type" : "integer", "pkey" : true},
                {"name" : "location", "type" : "text", "unique" : true, "indexed" : true},
                {"name" : "mimetype", "type" : "text", "not_null" : true, "indexed" : true},
                {"name" : "storetype", "type" : "text", "default" : "sqlite", "indexed" : true},
                {"name" : "hash", "type" : "text", "default" : null, "indexed" : true},
                {"name" : "created", "type" : "time"}
              ]
            }
            }
         )
    )
  end

  bootstrap

end
