# encoding: utf-8
# Copyright 2009-07-01, Massimo Maria Ghisalberti, Pragmas snc http://pragmas.org. All Rights Reserved.
# This is free software.
# licence: BSD 2-clause license ("Simplified BSD License" or "FreeBSD License") <http://www.freebsd.org/copyright/copyright.html>

# Ideas and basic implementations
# Massimo Maria Ghisalberti <massimo.ghisalberti@gmail.com, zairik@gmail.com, minimal.procedure@gmail.com >

# OxStore is a database key / value modeled on the metaphor of the human family
# relationships and spatial distribution.

# Features implemented or being implemented.
# Logical units are: Universe, Location, Home, Family, Members (of the family).
# The units are logical containers of underlying units and control relationships.
# Family members may be born, adopted, orphanized, rejected or killed.
# Each logical relationship can have an unlimited number of properties of different types.
#...

# STATE: BETA --

module OxPhysicalStoreSchemas

  require 'rubygems'
  require 'uuid'

  def init_schemas(collection)
    @collection = collection
    @universe = 'universe'
    @families = 'families'
    @orphans = 'orphans'
    @metadatas = 'metadatas'
    @family = @collection
    @member = @collection
  end

  def universe_schema
    init_schemas('universe')
    %(
    {
    "table" : {
      "name" : "#{@collection}",
      "columns" : [
        {"name" : "id", "type" : "integer", "pkey" : true},
        {"name" : "location", "type" : "text", "unique" : true, "indexed" : true},
        {"name" : "mimetype", "type" : "text", "not_null" : true, "indexed" : true},
        {"name" : "storetype", "type" : "text", "default" : "sqlite", "indexed" : true},
        {"name" : "hash", "type" : "text", "default" : null, "indexed" : true},
        {"name" : "created", "type" : "time"}
      ]
    }
    }
    )
  end

  def families_schema
    init_schemas('families')
    %(
    {
    "table" : {
      "name" : "#{@collection}",
      "columns" : [
        {"name" : "id", "type" : "integer", "pkey" : true},
        {"name" : "name", "type" : "text", "unique" : true, "indexed" : true},
        {"name" : "physical", "type" : "text", "unique" : true, "indexed" : true},
        {"name" : "mimetype", "type" : "text", "not_null" : true, "indexed" : true},
        {"name" : "storetype", "type" : "text", "default" : "sqlite", "indexed" : true},
        {"name" : "hash", "type" : "text", "default" : null, "indexed" : true},
        {"name" : "related", "type" : "text", "default" : null, "indexed" : true},
        {"name" : "created", "type" : "time"}
      ]
    }
    }
    )
  end

  def orphans_schema
    init_schemas('orphans')
    %(
    {
    "table" : {
      "name" : "#{@collection}",
      "columns" : [
        {"name" : "id", "type" : "integer", "pkey" : true},
        {"name" : "name", "type" : "text", "unique" : true, "indexed" : true},
        {"name" : "physical", "type" : "text", "unique" : true, "indexed" : true},
        {"name" : "mimetype", "type" : "text", "not_null" : true, "indexed" : true},
        {"name" : "storetype", "type" : "text", "default" : "sqlite", "indexed" : true},
        {"name" : "hash", "type" : "text", "default" : null, "indexed" : true},
        {"name" : "related", "type" : "text", "default" : null, "indexed" : true},
        {"name" : "created", "type" : "time"}
      ]
    }
    }
    )
  end

  def metadatas_schema
    init_schemas('metadatas')
    %(
        {
        "table" : {
          "name" : "#{@collection}",
          "columns" : [
            {"name" : "id", "type" : "integer", "pkey" : true},
            {"name" : "physical", "type" : "text", "unique" : true, "indexed" : true},
            {"name" : "feature", "type" : "text", "indexed" : true},
            {"name" : "definition", "type" : "text", "indexed" : true},
            {"name" : "mimetype", "type" : "text", "not_null" : true, "indexed" : true},
            {"name" : "storetype", "type" : "text", "default" : "sqlite", "indexed" : true},
            {"name" : "hash", "type" : "text", "default" : null, "indexed" : true}
          ]
        }
        }
        )
  end

  def family_schema
    uuid = UUID.new.generate(:compact)
    init_schemas("family_#{uuid}")
    %(
    {
    "table" : {
      "name" : "#{@collection}",
      "columns" : [
        {"name" : "id", "type" : "integer", "pkey" : true},
        {"name" : "name", "type" : "text", "unique" : true, "indexed" : true},
        {"name" : "physical", "type" : "text", "unique" : true, "indexed" : true},
        {"name" : "mimetype", "type" : "text", "not_null" : true, "indexed" : true},
        {"name" : "metainfo", "type" : "text", "indexed" : true},
        {"name" : "hash", "type" : "text", "default" : null, "indexed" : true},
        {"name" : "related", "type" : "text", "default" : null, "indexed" : true},
        {"name" : "created", "type" : "time"},
        {"name" : "updated", "type" : "time"}
      ]
    }
    }
    )
  end

  def member_schema
    uuid = UUID.new.generate(:compact)
    init_schemas("member_#{uuid}")
    %(
    {
    "table" : {
      "name" : "#{@collection}",
      "columns" : [
        {"name" : "id", "type" : "integer", "pkey" : true},
        {"name" : "feature", "type" : "text", "unique" : true, "indexed" : true},
        {"name" : "definition", "type" : "text", "indexed" : true},
        {"name" : "mimetype", "type" : "text", "not_null" : true, "indexed" : true},
        {"name" : "metainfo", "type" : "text", "indexed" : true},
        {"name" : "hash", "type" : "text", "default" : null, "indexed" : true},
        {"name" : "cdata", "type" : "text", "default" : null},
        {"name" : "created", "type" : "time"},
        {"name" : "updated", "type" : "time"}
      ]
    }
    }
    )
  end

  attr_accessor :universe, :families, :orphans, :family, :member, :metadatas
end

RUBY_INSTALL_NAME = RbConfig::CONFIG['RUBY_INSTALL_NAME']

if RUBY_PLATFORM =~ /java/
  require 'jdbc/sqlite3'
  require 'java'
  java_import Java::OrgSqlite::JDBC
else
  require 'sqlite3'
end

class OxPhysicalStoreProxy
  def initialize(store, storetype)
    @store = store
    @storetype = storetype
  end

  def store
    @store
  end

  def storetype
    @storetype
  end
end

class OxPhysicalConnection
  def self.connect(datastore, storetype = :sqlite)
    case storetype
    when :sqlite_memory
      store = SQLite3::Database.open(:memory)
    when :sqlite
      if RUBY_PLATFORM =~ /java/
        store = java.sql.DriverManager.getConnection(%(jdbc:sqlite:#{datastore}))
      else
        store = SQLite3::Database.open(datastore)
      end
    end
    OxPhysicalStoreProxy.new(store, storetype.to_s)
  end

end

class OxExecutionStatement
  #
  def initialize(conn)
    @execution = conn
  end

  if RUBY_PLATFORM =~ /java/

    private
    #
    def resultset(result_set)
      set = []
      columns = []
      meta_data = result_set.get_meta_data
      num_cols = meta_data.get_column_count
      (1..num_cols).each {|cn|
        columns.push(result_set.get_column_name(cn))
      }
      set.push(columns)

      while result_set.next
        record = []
        (1..num_cols).each {|cn|
          type_name = meta_data.get_column_type_name(cn).downcase.to_sym
          case type_name
          when :integer
            record.push(result_set.get_int(cn))
          when :text
            record.push(result_set.get_string(cn))
          end
        }
        set.push(record)
      end
      set
    end

    public

    def execute_batch(stat)
      stmt = @execution.store.createStatement
      stmt.executeUpdate(stat)
    end

    def execute(stat)
      stmt = @execution.store.createStatement
      stmt.execute(stat)
    end

    def execute_query(stat)
      stmt = @execution.store.createStatement
      resultset(stmt.executeQuery(stat))
    end

    def execute_update(stat)
      stmt = @execution.store.createStatement
      stmt.executeUpdate(stat)
    end

    def execute_insert(stat)
      stmt = @execution.store.createStatement
      stmt.executeUpdate(stat)
    end

    def execute_delete(stat)
      stmt = @execution.store.createStatement
      stmt.executeUpdate(stat)
    end

  else

    public

    def execute_batch(stat)
      @execution.store.execute_batch(stat)
    end

    def execute(stat)
      @execution.store.execute(stat)
    end

    def execute_query(stat)
      @execution.store.execute2(stat)
    end

    def execute_update(stat)
      @execution.store.execute2(stat)
    end

    def execute_insert(stat)
      @execution.store.execute2(stat)
    end

    def execute_delete(stat)
      @execution.store.execute2(stat)
    end

  end

  def close
    @execution.store.close
  end

  def closed?
    @execution.store.closed?
  end

end

class OxPhysicalStore
  def self.version; '0.6.0.0' end

  include OxPhysicalStoreSchemas

  private

  def normalize(value, on_store = true)
    if on_store
      value.gsub("'", "''")
    else
      value.gsub("''", "'")
    end
  end

  def house?
    if @house && !@house.closed?
      true
    else
      false
    end
  end

  def dataset(resultset)
    records = []
    resultset[1..resultset.size].each {|result|
      record = {}
      incr = 0
      resultset[0].each {|k|
        record[k.to_sym] = result[incr]
        incr += 1
      }
      records << record
    }
    records
  end

  def clause_builder(clause_hash = nil, and_or = false, comparison = :equal)
    if clause_hash
      compare = ''
      incr = 1
      size = clause_hash.size
      clause = ''
      clause_hash.each {|key,value|
        if and_or
          and_or_sep = ' AND '
        else
          and_or_sep = ', '
        end
        if size > 1 && incr < size
          and_stm = and_or_sep
        else
          if and_or
            and_stm = ';'
          else
            and_stm = ' '
          end
        end

        case comparison
        when :like
          comparison = ' LIKE '
        when :ilike
          comparison = '  ILIKE '
        when :equal
          comparison = '='
        else
          comparison = '='
        end
        value = normalize(value, true) if value.is_a?(String)
        clause += %(#{key.to_s} #{comparison} '#{value}' #{and_stm} )
        incr += 1
      }
      clause
    end
  end

  def where_builder(where = nil, comparison = :equal)
    if where && where.size > 0
      %( WHERE #{clause_builder(where, true, comparison)})
    else
      ''
    end
  end

  public

  def open(name = nil)
    @name = name if name
    return @house if house?
    conn = OxPhysicalConnection::connect(@name)
    @house = OxExecutionStatement.new(conn)
  end

  def close
    @house.close if house?
    @house = nil
  end

  def action_open
    @house.execute(%(BEGIN TRANSACTION;))
  end

  def action(statement, ddl = :query)
    open
    action_open
    case ddl
    when :ddl
      result = @house.execute_batch(statement)
    when :query
      result = @house.execute_query(statement)
    when :update
      result = @house.execute_update(statement)
    when :insert
      result = @house.execute_insert(statement)
    when :delete
      result = @house.execute_delete(statement)
    when :command
      result = @house.execute(statement)
    end
    action_close
    close
    result
  end

  def action_close
    @house.execute(%(COMMIT;))
  end

  def json_schema_to_sql(schema_json)

    schema = JSON::load(schema_json)
    return nil unless schema["table"]
    schema = schema["table"]
    columns = []
    indices = []
    schema["columns"].each {|col|
      column = ''
      index = ''
      column << %( '#{col["name"]}' )
      if col["type"]
        column << %( #{col["type"]} )
      else
        column << %( TEXT )
      end

      column << %( PRIMARY KEY ) if col["pkey"] && col["pkey"] == true
      column << %( UNIQUE ) if col["unique"] && col["unique"] == true
      column << %( NOT NULL ) if col["not_null"] && col["not_null"] == true

      if col["default"].is_a?(String)
        column << %( DEFAULT '#{col["default"]}' )
      else
        column << %( DEFAULT #{col["default"]} )
      end if col["default"]

      column << ",\n" unless schema["columns"].last == col

      if col["indexed"] && col["indexed"] == true
        index_option = col["index_option"] if col["index_option"]

        index = %(CREATE INDEX IF NOT EXISTS '#{schema["name"]}_#{col["name"]}_index' ON '#{schema["name"]}' ('#{col["name"]}' #{index_option});\n)
      end

      columns << column unless column == ''
      indices << index unless column == ''
    }

    create_stm = " CREATE TABLE IF NOT EXISTS '#{schema["name"]}' ( "

    columns.each {|col|
      create_stm << col
    }

    create_stm << ");\n"

    indices.each {|index|
      create_stm << index
    }
    create_stm
  end

  def prepare(schema)
    case schema
    when :universe
      action(json_schema_to_sql(metadatas_schema), :ddl)
      action(json_schema_to_sql(universe_schema), :ddl)
    when :location
      action(json_schema_to_sql(metadatas_schema), :ddl)
      action(json_schema_to_sql(families_schema), :ddl)
      action(json_schema_to_sql(orphans_schema), :ddl)
    when :family
      action(json_schema_to_sql(family_schema), :ddl)
    when :member
      action(json_schema_to_sql(member_schema), :ddl)
    else
      if schema.is_a?(String)
        action(json_schema_to_sql(schema), :ddl)
      else
        action(json_schema_to_sql(member_schema), :ddl)
      end
    end
    @collection
  end

  def inserts(fieldsets = [], collection = nil )
    collection = self.collection unless collection
    fieldsets.each{|fieldset|
      insert(fieldset, collection)
    }
  end

  def insert(values = {}, collection = nil)
    collection = self.collection unless collection
    fields = values.keys.map{  |k| %('#{k}') }.join(',')
    values = values.values.map{|v|
      v = normalize(v, true) if v.is_a?(String)
      %('#{v}')
    }.join(',')
    insert_stm = %(
    INSERT INTO #{collection}
      (#{fields})
    VALUES
      (#{values});
    )
    action(insert_stm, :insert)
  end

  def update(fieldsets = [], values = [], collection = nil)
    collection = self.collection unless collection
    fsets = []
    vals = []
    unless fieldsets.is_a?(Array)
      fsets.push(fieldsets)
    else
      fsets = fieldsets
    end
    unless values.is_a?(Array)
      vals.push(values)
    else
      fsets = fieldsets
    end
    incr = 0
    fsets.each {|fieldset|
      clause = where_builder(fieldset)
      newvalues = clause_builder(vals[incr])
      upd_stm = %(UPDATE #{collection} SET #{newvalues} #{clause})
      #p upd_stm
      action(upd_stm, :update)
      incr += 1
    }
  end

  def filter_by_script(find_stm)
    dataset(action(find_stm))
  end

  def filter(where = nil, fields = [], collection = nil, options = {}) #limit = false)
    limit = false
    order_by = ''
    limit = options if options.is_a?(Fixnum)
    if options.is_a?(Hash)
      limit = options[:limit] if options[:limit]
      order_by = %(ORDER BY #{options[:order_by]}) if options[:order_by]
    end

    collection = self.collection unless collection
    limit_to = ''
    if limit
      limit_to = %( LIMIT #{limit.to_i} )
    end
    clause = ''
    clause = where_builder(where)
    if fields.size > 0
      fields = fields.map {|f| %(#{f.to_s})}
      fields = fields.join(', ')
      find_stm = %(SELECT #{fields} FROM #{collection} #{clause} #{order_by} #{limit_to})
    else
      find_stm = %(SELECT * FROM #{collection} #{clause} #{order_by} #{limit_to})
    end
    #p find_stm
    dataset(action(find_stm))
  end

  def delete(fieldsets, collection)
    fields = []
    unless fieldsets.is_a?(Array)
      fields.push(fieldsets)
    else
      fields = fieldsets
    end
    fields.each {|field|
      clause = where_builder(field)
      del_stm = %(DELETE FROM #{collection} #{clause} )
      action(del_stm, :delete)
    }
  end

  def drop(collection)
    drop_stm = %(DROP TABLE IF EXISTS #{collection})
    action(drop_stm, :update)
  end

  def compact
    compact_stm = %(VACUUM;)
    action(compact_stm, :update)
  end

  def self.start(house = 'test.store')
    ox_physical = OxPhysicalStore.new
    ox_physical.open(house)
    ox_physical
  end

  def self.destroy(name)
    File.unlink(name) if File.exist?(name)
  end

  attr_reader :house, :name
  attr_accessor :collection

end

if __FILE__ == $0
  def bootstrap
  end

  bootstrap

end
