# encoding: utf-8
# Copyright 2009-07-01, Massimo Maria Ghisalberti, Pragmas snc http://pragmas.org. All Rights Reserved.
# This is free software.
# licence: BSD 2-clause license ("Simplified BSD License" or "FreeBSD License") <http://www.freebsd.org/copyright/copyright.html>

# Ideas and basic implementations
# Massimo Maria Ghisalberti <massimo.ghisalberti@gmail.com, zairik@gmail.com, minimal.procedure@gmail.com >

# OxStore is a database key / value modeled on the metaphor of the human family
# relationships and spatial distribution.

# Features implemented or being implemented.
# Logical units are: Universe, Location, Home, Family, Members (of the family).
# The units are logical containers of underlying units and control relationships.
# Family members may be born, adopted, orphanized, rejected or killed.
# Each logical relationship can have an unlimited number of properties of different types.
#...

# STATE: BETA --

%w[
.
].each do |dep|
  $LOAD_PATH.unshift(File.dirname(__FILE__) + "/#{dep}")
end

require 'rubygems'
require 'uuid'
require 'json'
require "digest"
require "zlib"
require 'base64'
require 'openssl'
require 'ox_physical_store'
require 'yaml'

module OxStore
  def self.version; '0.6.1.0' end

  def self.configuration(*args)
    confpath = File.join(File.dirname(__FILE__), '../conf/configuration.yml')
    if File.exist?(confpath)
      conf = YAML::load(File.open(confpath))
      args.each {|arg| conf = conf[arg.to_s]}
      conf
    else
      nil
    end
  end

  OBJECT_STATUS_ACTIVE = :active
  OBJECT_STATUS_INACTIVE = nil
  HOME_UNKNOWN = nil
  FAMILY_UNKNOWN = nil
  LOCATION_UNKNOWN = nil
  UNIVERSE_UNKNOWN = nil

  MIME_TEXT_UNIVERSE = 'text/universe'
  MIME_TEXT_LOCATION = 'text/location'
  MIME_TEXT_FAMILY = 'text/family'

  MIME_TEXT_FAMILY_MEMBER = 'text/family/member'
  MIME_TEXT_FEATURE_PLAIN = 'text/feature/plain'
  MIME_TEXT_FEATURE_HTML = 'text/feature/html'
  MIME_TEXT_FEATURE_XHTML = 'text/feature/xhtml'
  MIME_TEXT_FEATURE_CSS = 'text/feature/css'
  MIME_TEXT_FEATURE_HAML = 'text/feature/haml'
  MIME_TEXT_FEATURE_SASS = 'text/feature/sass'
  MIME_TEXT_FEATURE_RUBY = 'text/feature/ruby'
  MIME_TEXT_FEATURE_RUBY_INSIDE = 'text/feature/ruby/inside'
  MIME_TEXT_FEATURE_JSON = 'text/feature/json'
  MIME_TEXT_FEATURE_YAML = 'text/feature/yaml'
  MIME_TEXT_FEATURE_FILE = 'text/feature/file'

  MIME_TEXT_META_PLAIN = 'text/meta/plain'
  MIME_NUMBER_META_PLAIN = 'number/meta/plain'
  MIME_NUMBER_META_FLOAT = 'number/meta/float'

  MIME_BINARY_FEATURE_PLAIN = 'binary/feature/plain'
  MIME_BINARY_FEATURE_JPEG = 'binary/feature/image:jpg'
  MIME_BINARY_FEATURE_PNG = 'binary/feature/image:png'
  MIME_BINARY_FEATURE_GIF = 'binary/feature/image:gif'
  MIME_BINARY_FEATURE_PDF = 'binary/feature/image:pdf'
  MIME_BINARY_FEATURE_SWF = 'binary/feature/image:swf'

  MIME_CODE_FEATURE_RUBY = 'code/feature/ruby'

  MIME_DATASETS_FEATURE_FILTER = 'datasets/feature/filter'
  MIME_DATASETS_FEATURE_JSON = 'datasets/feature/json'
  MIME_DATASETS_FEATURE_SCRIPT = 'datasets/feature/script'
  MIME_DATASETS_FEATURE_STORED = 'datasets/feature/stored'

  MIME_COMPRESSED = '/compressed'
  MIME_ENCRYPTED = '/encrypted'

  ENCRYPTION_SCHEMA = ':rsa'
  COMPRESSION_SCHEMA = ':deflate'

  MIME_FEATURE_ENCRYPTED = MIME_ENCRYPTED + ENCRYPTION_SCHEMA
  MIME_FEATURE_COMPRESSED = MIME_COMPRESSED + COMPRESSION_SCHEMA
  MIME_FEATURE_ENCRYPTED_COMPRESSED = MIME_FEATURE_ENCRYPTED + MIME_FEATURE_COMPRESSED
end

module OxStoreTools

  private
  def json(data)
    data.to_json
  end

  def normalize_array_of_hash(data, norm = :symbol)
    data.map{ |data|
      if data.is_a?(Hash)
        data = normalize_hash_keys(data, norm)
      elsif data.is_a?(Array)
        normalize_array_of_hash(data, norm)
      end
    }
  end

  def json_load(data)
    data = JSON::load(data)
    if data.is_a?(Hash)
      data = normalize_hash_keys(data, :symbol)
    elsif data.is_a?(Array)
      data = normalize_array_of_hash(data, :symbol)
    end
    data
  end

  def definition_hash(definition)
    #digest = Digest::MD5.new
    digest = Digest::SHA2.new
    digest.update(definition)
    digest.hexdigest
  end

  def normalize_name(classname)
    if
    classname.is_a?(OxUniverse) ||
    classname.is_a?(OxLocation) ||
    classname.is_a?(OxFamily) ||
    classname.is_a?(OxMember)
      classname.name
    else
      classname
    end
  end

  def normalize_hash_keys(hash, norm = :symbol)
    rehash = {}
    if norm == :symbol
      hash.each{|key, value|
        if value.is_a?(Hash)
          rehash[key.to_sym] = normalize_hash_keys(value, norm)
        else
          rehash[key.to_sym] = value
        end
      }
    else
      hash.each{|key, value|
        if value.is_a?(Hash)
          rehash[key.to_s] = normalize_hash_keys(value, norm)
        else
          rehash[key.to_s] = value
        end
      }
    end
    rehash
  end

  def crypt(data)
    return nil unless data
    pkey = OxStore.configuration('cryptation', 'public_key')
    unless pkey =~ %r{^http:\/\/|^ftp:\/\/|^\/|^\w:\\}
      pkey = File.join(File.dirname(__FILE__), '../keys', pkey)
    end
    public_key_block = ''
    open(pkey) {|f| public_key_block = f.read} if pkey
    public_key =  OpenSSL::PKey::RSA.new(public_key_block)
    public_key.public_encrypt(data)
  end

  def decrypt(data)
    return nil unless data
    password = OxStore.configuration('cryptation', 'password')
    pkey = OxStore.configuration('cryptation', 'private_key')
    unless pkey =~ %r{^http:\/\/|^ftp:\/\/|^\/|^\w:\\}
      pkey = File.join(File.dirname(__FILE__), '../keys', pkey)
    end
    private_key_block = ''
    open(pkey) {|f| private_key_block = f.read} if pkey
    private_key = OpenSSL::PKey::RSA.new(private_key_block, password)
    private_key.private_decrypt(data)
  end

  def encode(data)
    return nil unless data
    Base64.encode64(data)
  end

  def decode(data)
    return nil unless data
    Base64.decode64(data)
  end

  def compress(data)
    return nil unless data
    Zlib::Deflate.deflate(data, Zlib::BEST_COMPRESSION)
  end

  def decompress(data)
    return nil unless data
    data = Zlib::Inflate.inflate(data)
  end

  def decode_mime(mime)
    mime.split('/').map {|t|
      st = t.split(':')
      if st.size > 1
        st
      else
        t
      end
    }
  end

  def mimefind(mime, tmime)
    mime = decode_mime(mime)
    result = []
    mime.each { |m|
      if m.is_a?(Array)
        if m.include?(tmime)
          result[0] = mime.index(m)
          result[1] = m.index(tmime)
          break
        end
      else
        if m == tmime
          result[0] = mime.index(m)
          break
        end
      end
    }
    result
  end

  public

  def metadefine(feature, definition, mimetype = OxStore::MIME_TEXT_META_PLAIN )
    @house.insert({
      :physical => @home,
      :feature  => feature,
      :definition  => definition,
      :mimetype  => mimetype,
      :hash  => definition_hash(definition)
    }, @house.metadatas)
  end

  def metareject(feature)
    @house.delete(metadata(feature), @house.metadatas)
  end

  def metasreject
    @house.delete(metadatas, @house.metadatas)
  end

  def metadata(feature)
    @house.filter({:physical => @home, :feature => feature }, ['*'], @house.metadatas, 1)
  end

  def metadatas
    @house.filter({:physical => @home}, ['*'], @house.metadatas)
  end

end

class OxUniverse

  include OxStore
  include OxStoreTools

  private
  def find_location(location, as_universe = false)
    mtype = MIME_TEXT_LOCATION
    mtype = MIME_TEXT_UNIVERSE if as_universe
    @house.filter({:location => location, :mimetype => mtype}, [:location] )
  end

  public

  def initialize(universe = "universe.sqlite", options = { :force => false })
    OxPhysicalStore.destroy(universe) if  options[:force] == true
    @name = universe
    @home = universe
    @house = OxPhysicalStore.start(universe)
    @house.prepare(:universe) if options[:force] == true
    @status = OBJECT_STATUS_ACTIVE
  end

  def adopt(location, as = :location, storetype = :sqlite)
    case as
    when :location
      mtype = MIME_TEXT_LOCATION
    when :universe
      mtype = MIME_TEXT_UNIVERSE
    else
      mtype = MIME_TEXT_LOCATION
    end
    location = normalize_name(location)
    loc = find_location(location)
    if loc.size == 0
      @house.insert({
        :location => location,
        :mimetype => mtype,
        :storetype => storetype.to_s,
        :hash => definition_hash(location),
        :created => Time.now
      })
      find_location(location)
    end
  end

  def reject(location)
    location = normalize_name(location)
    loc = find_location(location)
    if loc.size > 0
      @house.delete(loc, @house.universe)
    end
  end

  def location(location)
    location = normalize_location(location)
    loc = find_location(location)
    if loc.size > 0
      OxLocation.new(loc.first[:location].to_s, {:force => false})
    else
      nil
    end
  end

  attr_reader :house, :name, :home
  attr_accessor :status

end

class OxLocation

  include OxStore
  include OxStoreTools

  private
  def find_family(name, from = :orphans)
    case from
    when :orphans
      result = @house.filter({:name => name, :mimetype=>MIME_TEXT_FAMILY}, ['*'], @house.orphans, 1)
    when :families
      result = @house.filter({:name => name, :mimetype=>MIME_TEXT_FAMILY}, ['*'], @house.families, 1)
    else
      result = @house.filter({:name => name, :mimetype=>MIME_TEXT_FAMILY}, ['*'], from, 1)
    end
    result
  end

  def find_families(from = :orphans)
    case from
    when :orphans
      result = @house.filter({:mimetype=>MIME_TEXT_FAMILY}, ['*'], @house.orphans)
    when :families
      result = @house.filter({:mimetype=>MIME_TEXT_FAMILY}, ['*'], @house.families)
    else
      result = @house.filter({:mimetype=>MIME_TEXT_FAMILY}, ['*'], from)
    end
    result
  end

  def delete(family, bypass_dataset = false)
    return nil unless @house
    unless bypass_dataset
      name = normalize_name(family)
      fam = find_family(name, orphans = false)
    else
      fam = family
    end
    if fam.size > 0
      @house.delete(fam, @house.families)
      @house.drop(fam.first[:physical])
    end
  end

  def orphanize(family, bypass_dataset = false)
    return nil unless @house
    unless bypass_dataset
      name = normalize_name(family)
      fam = find_family(name, orphans = false)
    else
      fam = family
    end
    if fam.size > 0
      @house.insert({
        :name => fam.first[:name],
        :physical => fam.first[:physical],
        :mimetype => fam.first[:mimetype],
        :hash => definition_hash(fam.first[:name]),
        :related => fam.first[:related],
        :created => Time.now
      }, @house.orphans)
      @house.delete(fam, @house.families)
      find_family(fam.first[:name], orphans = true)
    end
  end

  public

  def initialize(location = "location.store", options = { :force => false })
    File.unlink(location) if File.exist?(location) && options[:force] == true
    @name = location
    @home = location
    @house = OxPhysicalStore.start(location)
    @house.prepare(:location) if options[:force] == true
    @status = OBJECT_STATUS_ACTIVE
  end

  def adopt(family, options = {})
    return nil unless @house
    name = normalize_name(family)
    fam = find_family(name, :orphans)
    if fam.size > 0
      fam = fam[0]
      @house.insert({
        :name => fam[:name],
        :physical => fam[:physical],
        :mimetype => MIME_TEXT_FAMILY,
        :hash => definition_hash(fam[:name]),
        :related => nil,
        :created => Time.now
      }, @house.families)
      @house.delete(fam, @house.orphans)
      find_family(name, :families)
    end
  end

  def reject(family, options = {:kill => false})
    return nil unless @house
    name = normalize_name(family)
    fam = find_family(name, orphans = false)
    if fam.size > 0
      if options[:kill] == true
        family.status = OBJECT_STATUS_INACTIVE
        family.home = HOME_UNKNOWN
        family.location = LOCATION_UNKNOWN
        delete(fam, true)
      else
        orphanize(fam, true)
        family.location = 'orphans'
      end
    end
  end

  def family(name)
    find_family(name, false)
  end

  def families
    find_families(false)
  end

  def orphan(name)
    find_family(name, true)
  end

  def orphans
    find_families(true)
  end

  def instance(family)
    name = normalize_name(family)
    dataset = find_family(name, :families)
    if dataset.size > 0
      fam = OxFamily.new
      fam.house = @house
      fam.relink(dataset.first[:name], :location)
    else
      nil
    end
  end

  def create(name)
    family = OxFamily.new(name, @name, {:force => true})
    adopt(family)
    family
  end

  def clone(family, name, deep = :full)
    family_name = normalize_name(family)
    fam = find_family(family_name, :families)
    if fam.size > 0
      family = OxFamily.new(name, @name, {:force => true})
      fam = fam[0]
      fam = instance(fam[:name])
      case deep
      when :full
        family.import(fam.datasets(:all))
      when :light
        sets = fam.datasets(:all)
        sets.each{|set|
          set[:metainfo] = {}.to_json
          set[:created] = Time.now
          set[:updated] = Time.now
        }
        family.import(sets)
      end
    end
    adopt(family)
    family
  end

  attr_reader :house, :name, :home
  attr_accessor :status

end

class OxFamily

  include OxStore
  include OxStoreTools

  private
  def family(name = 'family', location = nil, options = { :force => false })
    if name && location
      location = normalize_name(location)
      @location = 'families'
      @name = name
      @house_name = location
      @house = OxPhysicalStore.start(location)
      @house.prepare(:family) if options[:force] == true
      @home = @house.family
      @house.insert({
        :name => name,
        :physical => @house.family,
        :hash => definition_hash(name),
        :mimetype => MIME_TEXT_FAMILY,
        :related => nil,
        :created => Time.now
      }, @house.orphans)
    else
      []
    end
  end

  def member_to_family(member_name, member_uuid, type = MIME_TEXT_FAMILY_MEMBER, metainfo = {}, options = {})
    {
      :physical => member_uuid,
      :name => member_name,
      :mimetype => type,
      :hash => definition_hash(member_name),
      :metainfo => metainfo.to_json,
      :related => nil,
      :created => Time.now,
      :updated => Time.now
    }
  end

  def find_member(name, from = :home)
    case from
    when :home
      result = @house.filter({:name => name, :mimetype=> MIME_TEXT_FAMILY_MEMBER}, ['*'], @home, 1)
    when :orphans
      result = @house.filter({:name => name, :mimetype=> MIME_TEXT_FAMILY_MEMBER}, ['*'], @house.orphans, 1)
    else
      result = @house.filter({:name => name, :mimetype=> MIME_TEXT_FAMILY_MEMBER}, ['*'], from, 1)
    end
    result
  end

  def find_self(name, from = :location, by_physical = false )
    case from
    when :orphans
      if by_physical
        result = @house.filter({:physical => name, :mimetype=> MIME_TEXT_FAMILY}, ['*'], @house.orphans, 1)
      else
        result = @house.filter({:name => name, :mimetype=> MIME_TEXT_FAMILY}, ['*'], @house.orphans, 1)
      end
    when :location, :families
      if by_physical
        result = @house.filter({:physical => name, :mimetype=> MIME_TEXT_FAMILY}, ['*'], @house.families, 1)
      else
        result = @house.filter({:name => name, :mimetype=> MIME_TEXT_FAMILY}, ['*'], @house.families, 1)
      end
    else
      if by_physical
        result = @house.filter({:physical => name, :mimetype=> MIME_TEXT_FAMILY}, ['*'], from, 1)
      else
        result = @house.filter({:name => name, :mimetype=> MIME_TEXT_FAMILY}, ['*'], from, 1)
      end
    end
    result
  end

  def delete(member, bypass_dataset = false)
    return nil unless @house
    unless bypass_dataset
      name = normalize_name(member)
      mbr = find_member(name)
    else
      mbr = member
    end
    if mbr.size > 0
      @house.delete(mbr, @home)
      @house.drop(mbr.first[:physical])
    end
  end

  def orphanize(member, bypass_dataset = false)
    return nil unless @house
    unless bypass_dataset
      name = normalize_name(member)
      mbr = find_member(name)
    else
      mbr = member
    end
    if mbr.size > 0
      @house.insert({
        :name => mbr.first[:name],
        :physical => mbr.first[:physical],
        :hash => definition_hash(mbr.first[:name]),
        :mimetype => mbr.first[:mimetype],
        :related => mbr.first[:related],
        :created => Time.now
      }, @house.orphans)
      @house.delete(mbr, @home)
      find_member(mbr.first[:name], :orphans)
    end
  end

  public

  def initialize(name = 'family', location = nil, options = { :force => false })
    @location = LOCATION_UNKNOWN
    @status = OBJECT_STATUS_INACTIVE
    if name
      family(name, location, options)
      @status = OBJECT_STATUS_ACTIVE
    end
  end

  def adopt(member, mimetype = MIME_TEXT_FAMILY_MEMBER, metainfo = {}, options = {})
    return nil unless @house
    member.family = @home
    name = normalize_name(member)
    orph = find_member(name, :orphans)
    if orph.size > 0
      table_uuid = orph.first[:physical]
      @house.insert(
      member_to_family(name, table_uuid, mimetype, metainfo, options),
      @home
      )
      @house.delete(orph, @house.orphans)
      find_member(name)
    end
  end

  def reject(member, options = {:kill => false})
    return nil unless @house
    member.family = nil
    name = normalize_name(member)
    mbr = find_member(name)
    if mbr.size > 0
      if options[:kill] == true
        member.status = OBJECT_STATUS_INACTIVE
        member.home = HOME_UNKNOWN
        member.family = FAMILY_UNKNOWN
        delete(mbr, true)
      else
        member.family = FAMILY_UNKNOWN
        orphanize(mbr, true)
      end
    end
  end

  def recycle(oxmember, member)
    if oxmember.status == OBJECT_STATUS_INACTIVE
      oxmember.relink(member)
    else
      oxmember
    end
  end

  def instance(member)
    name = normalize_name(member)
    dataset = find_member(name)
    if dataset.size > 0
      mbr = OxMember.new
      mbr.house = @house
      mbr.family = @home
      mbr.relink(dataset.first[:name], :family)
    else
      nil
    end
  end

  def rename(oldname, newname)
    oldname = normalize_name(oldname)
    mbr = find_member(oldname)
    if mbr.size > 0
      @house.update(mbr,
      {
        :name => newname,
        :hash => definition_hash(newname),
        :updated => Time.now
      }, @home)
      find_member(newname)
    end
  end

  def create(name)
    member = OxMember.new(name, @house_name)
    adopt(member)
    member
  end

  def clone(member, name, deep = :full, to_family = nil)
    if to_family
      _self_ = to_family
    end
    _self_ = self
    member_name = _self_.send(:normalize_name, member)
    mbr = _self_.send(:find_member, member)
    if mbr.size > 0
      member = OxMember.new(name, _self_.house_name)
      mbr = mbr[0]
      mbr = _self_.instance(mbr[:name])
      case deep
      when :full
        member.import(mbr.datasets(:all))
      when :light
        sets = mbr.datasets(['feature, mimetype'])
        sets.each{|set|
          set[:created] = Time.now
          set[:updated] = Time.now
        }
        member.import(sets)
      end
    end
    _self_.adopt(member)
    member
  end

  def datasets(*args)
    def assign_value(val)
      result = {}
      if val.is_a?(Fixnum)
        result[:limit] = val
      elsif val.is_a?(String) || val.is_a?(Symbol)
        if val == :asc || val == :desc
          result[:order] = val.to_s
        elsif val == :restricted || val == :all
          result[:alldata] = val
        end
      elsif val.is_a?(Array)
        result[:alldata] = val
      end
      result
    end
    args_opts = {
      :alldata => :restricted,
      :order => 'ASC',
      :limit => nil
    }

    arg_num = args.size
    case arg_num
    when 1
      args_opts.merge!(assign_value(args[0]))
    when 2
      args_opts.merge!(assign_value(args[0]))
      args_opts.merge!(assign_value(args[1]))
    when 3
      args_opts.merge!(assign_value(args[0]))
      args_opts.merge!(assign_value(args[1]))
      args_opts.merge!(assign_value(args[2]))
    end if arg_num <= 3
    options = {}
    if args_opts[:alldata].is_a?(Array)  &&
    args_opts[:alldata].size > 0 &&
    args_opts[:alldata][0] != '*'
      options[:order_by] = "#{args_opts[:alldata][0]} #{args_opts[:order].to_s}" if args_opts[:order]
    else
      options[:order_by] = "name #{args_opts[:order].to_s}" if args_opts[:order]
    end
    options[:limit] = args_opts[:limit] if args_opts[:limit]
    if args_opts[:alldata].is_a?(Array)
      @house.filter({}, args_opts[:alldata], @home, options)
    else
      if args_opts[:alldata] == :all
        @house.filter({}, ['*'], @home, options)
      else
        @house.filter({}, ['name', 'physical', 'mimetype'], @home, options)
      end
    end
  end

  def import(datasets)
    datasets.each {|set|
      @house.insert(set, @home)
    }
  end

  def relink(dataset, from = :location)
    name = normalize_name(dataset)
    _self =  find_self(name, from)
    if _self.size > 0
      _self = _self[0]
      @location = 'families'
      @name = _self[:name]
      @home = _self[:physical]
      @house.member = _self[:physical]
      @house.family = _self[:physical]
      @house.collection = _self[:physical]
      @house_name = @house.name
      @status = OBJECT_STATUS_ACTIVE
    end
    self
  end

  def related(relation)
    name = normalize_name(relation)
    relation = @house.filter({:name => name}, [:name], @house.families, 1)
    if relation.size > 0
      result = @house.filter({:name => @name}, [:name], @location, 1)
      @house.update(result, {:related => name}, @location)
    else
      nil
    end
  end

  def relation
    result = @house.filter({:name => @name}, [:related], @location, 1)
    if result[0][:related].empty?
      nil
    else
      result[0][:related]
    end
  end

  attr_reader :name, :house_name, :home
  attr_accessor :location, :status, :house

end

class OxMember

  include OxStore
  include OxStoreTools

  private
  #at birth member is orphan
  def birth(name, location, metainfo = {}, options = {})
    if name && location
      location = normalize_name(location)
      @house = OxPhysicalStore.start(location)
      @house_name = location
      @house.prepare(:member)
      @home = @house.member
      @name = name
      @house.insert({
        :name => name,
        :physical => @house.member,
        :hash => definition_hash(name),
        :mimetype => MIME_TEXT_FAMILY_MEMBER,
        :related => nil,
        :created => Time.now
      }, @house.orphans)
    else
      []
    end
  end

  def find_self(name, from = :family, by_physical = false )
    case from
    when :orphans
      if by_physical
        result = @house.filter({:physical => name, :mimetype=> MIME_TEXT_FAMILY_MEMBER}, ['*'], @house.orphans, 1)
      else
        result = @house.filter({:name => name, :mimetype=> MIME_TEXT_FAMILY_MEMBER}, ['*'], @house.orphans, 1)
      end
    when :family
      if by_physical
        result = @house.filter({:physical => name, :mimetype=> MIME_TEXT_FAMILY_MEMBER}, ['*'], @family, 1)
      else
        result = @house.filter({:name => name, :mimetype=> MIME_TEXT_FAMILY_MEMBER}, ['*'], @family, 1)
      end
    else
      if by_physical
        result = @house.filter({:physical => name, :mimetype=> MIME_TEXT_FAMILY_MEMBER}, ['*'], from, 1)
      else
        result = @house.filter({:name => name, :mimetype=> MIME_TEXT_FAMILY_MEMBER}, ['*'], from, 1)
      end
    end
    result
  end

  def feature_exist?(feature)
    feat = @house.filter( {:feature => feature}, ['*'], @home)
    if feat.size > 0
      feat
    else
      false
    end
  end

  def normalize_definition(definition, on_store = true)
    if on_store
      definition.gsub("'", "''")
    else
      definition.gsub("''", "'")
    end
  end

  def process_definition_on_define(definition, mimetype)

    require "open-uri"

    if definition.is_a?(String) && definition.match(/\^file\:\/\//u)
      filepath = definition.gsub("file://", "")
      if File.exist?(filepath)
        File.open(filepath, 'r'){ |f|
          definition = f.read
        }
      end
    end

    if definition.is_a?(String) && definition.match(/\^http\:\/\//u)
      open(definition){ |f|
        definition = f.read
      }
    end
    if definition.is_a?(String) && definition.match(/\^https\:\/\//u)
      open(definition){ |f|
        definition = f.read
      }
    end
    if definition.is_a?(String) && definition.match(/\^ftp\:\/\//u)
      open(definition){ |f|
        definition = f.read
      }
    end

    case mimetype
    when MIME_DATASETS_FEATURE_FILTER
      definition = definition.to_json
    when MIME_DATASETS_FEATURE_STORED
      definition = definition.to_json
    end

    binary = mimefind(mimetype, 'binary')
    encrypted =  mimefind(mimetype, 'encrypted')
    compressed = mimefind(mimetype, 'compressed')

    if encrypted.size > 0 && compressed.size > 0
      binary = []
      definition = encode(compress(crypt(definition)))
    elsif encrypted.size > 0
      binary = []
      definition = encode(crypt(definition))
    elsif compressed.size > 0
      binary = []
      definition = encode(compress(definition))
    end

    if binary.size > 0
      definition = encode(definition)
    end
    normalize_definition(definition)
  end

  def process_definition_on_read(definition, mimetype)

    binary = mimefind(mimetype, 'binary')
    encrypted =  mimefind(mimetype, 'encrypted')
    compressed = mimefind(mimetype, 'compressed')
    if encrypted.size > 0 && compressed.size > 0
      binary = []
      definition = decrypt(decompress(decode(definition)))
      mimetype.gsub!(OxStore::MIME_FEATURE_ENCRYPTED_COMPRESSED, '')
    elsif encrypted.size > 0
      binary = []
      definition = decrypt(decode(definition))
      mimetype.gsub!(OxStore::MIME_FEATURE_ENCRYPTED, '')
    elsif compressed.size > 0
      binary = []
      definition = decompress(decode(definition))
      mimetype.gsub!(OxStore::MIME_FEATURE_COMPRESSED, '')
    end

    if binary.size > 0
      definition = decode(definition)
    end
    definition = normalize_definition(definition, false)

    case mimetype

    when MIME_DATASETS_FEATURE_FILTER, MIME_DATASETS_FEATURE_JSON
      definition = json_load(definition)

      unless definition[:results]
        results = ['*']
      else
        results = definition[:results]
      end

      if definition[:location]
        house = OxPhysicalStore.start(definition[:location])
        definition = house.filter(definition[:filter], results, definition[:collection])
      else
        definition = @house.filter(definition[:filter], results, definition[:collection])
      end

    when MIME_DATASETS_FEATURE_STORED
      definition = json_load(definition)

    when MIME_DATASETS_FEATURE_SCRIPT
      if definition.is_a?(Hash) && definition[:location]
        house = OxPhysicalStore.start(definition[:location])
        definition = house.filter_by_script(definition)
      else
        definition = @house.filter_by_script(definition)
      end

    when MIME_CODE_FEATURE_RUBY
      definition = Object.class_eval(definition)

    when MIME_TEXT_FEATURE_RUBY_INSIDE
      definition = OxMember.class_eval(definition)

    end

    definition
  end

  public

  def initialize(name = nil, location = nil, metainfo = {}, options = {})
    @family = FAMILY_UNKNOWN
    @status = OBJECT_STATUS_INACTIVE
    if name
      birth(name, location, metainfo, options)
      @status = OBJECT_STATUS_ACTIVE
    end
  end

  def relink(dataset, from = :family)
    name = normalize_name(dataset)
    _self =  find_self(name, from)
    if _self.size > 0
      @name = _self.first[:name]
      @home = _self.first[:physical]
      @house.member = @home
      @house.family = @home
      @house.collection = @home
      @house_name = @house.name
      @status = OBJECT_STATUS_ACTIVE
    end
    self
  end

  def import(datasets)
    datasets.each {|set|
      @house.insert(set, @home)
    }
  end

  def define(feature, definition, mimetype = nil, metainfo = nil, cdata = nil, options = {})
    dataset = feature_exist?(feature)
    unless dataset

      unless mimetype
        mimetype = MIME_TEXT_FEATURE_PLAIN
      end
      unless metainfo
        metainfo = {}
      end
      definition = process_definition_on_define(definition, mimetype)
      @house.insert({
        :feature  => feature,
        :definition  => definition,
        :mimetype  => mimetype,
        :hash  => definition_hash(definition),
        :cdata  => cdata,
        :metainfo  => metainfo.to_json,
        :created  => Time.now,
        :updated  => Time.now
      }, @home)
    else
      dataset = dataset[0]
      unless mimetype
        mimetype = dataset[:mimetype]
      end
      unless metainfo
        metainfo = json_load(dataset[:metainfo])
      end
      unless cdata
        cdata = dataset[:cdata]
      end
      definition = process_definition_on_define(definition, mimetype)
      @house.update(dataset,
      {
        :definition  => definition,
        :mimetype  => mimetype,
        :hash  => definition_hash(definition),
        :cdata  => cdata,
        :metainfo  => metainfo.to_json,
        :updated  => Time.now
      }, @home)
    end
  end

  def read(feature, store_to = nil, override_mime = true)
    dataset = @house.filter({:feature => feature}, [:definition, :mimetype], @home, 1)
    if dataset.size > 0
      dataset = dataset[0]
      definition = process_definition_on_read(dataset[:definition], dataset[:mimetype])
      if store_to
        unless override_mime
          mime = decode_mime(dataset[:mimetype])
          if (binary = mimefind(dataset[:mimetype], 'image')).size > 0 ||
          (binary = mimefind(dataset[:mimetype], 'document')).size > 0 ||
          (binary = mimefind(dataset[:mimetype], 'raw')).size > 0
            store_to = %(#{store_to}.#{mime[2][1]})
          end
        end
        File.open(store_to, 'w'){|f| f << definition}
        definition = store_to
      end
    else
      definition = nil
    end
    definition
  end

  def mimetype(feature)
    dataset = @house.filter({:feature => feature}, [:mimetype], @home, 1)
    if dataset.size > 0
      dataset[0][:mimetype]
    else
      nil
    end
  end

  def hash(feature)
    dataset = @house.filter({:feature => feature}, [:hash], @home, 1)
    if dataset.size > 0
      dataset[0][:hash]
    else
      nil
    end
  end

  def cdata(feature)
    dataset = @house.filter({:feature => feature}, [:cdata], @home, 1)
    if dataset.size > 0
      dataset[0][:cdata]
    else
      nil
    end
  end

  def metainfo(feature)
    dataset = @house.filter({:feature => feature}, [:metainfo], @home, 1)
    if dataset.size > 0
      json_load(dataset[0][:metainfo])
    else
      nil
    end
  end

  def created(feature)
    dataset = @house.filter({:feature => feature}, [:created], @home, 1)
    if dataset.size > 0
      dataset[0][:created]
    else
      nil
    end
  end

  def updated(feature)
    dataset = @house.filter({:feature => feature}, [:updated], @home, 1)
    if dataset.size > 0
      dataset[0][:updated]
    else
      nil
    end
  end

  def delete(feature)
    if feature.is_a?(String)
      feature = {:feature => feature}
    end
    @house.delete(feature, @home)
  end

  def datasets(*args)
    def assign_value(val)
      result = {}
      if val.is_a?(Fixnum)
        result[:limit] = val
      elsif val.is_a?(String) || val.is_a?(Symbol)
        if val == :asc || val == :desc
          result[:order] = val.to_s
        elsif val == :restricted || val == :all
          result[:alldata] = val
        end
      elsif val.is_a?(Array)
        result[:alldata] = val
      end
      result
    end
    args_opts = {
      :alldata => :restricted,
      :order => 'ASC',
      :limit => nil
    }

    arg_num = args.size
    case arg_num
    when 1
      args_opts.merge!(assign_value(args[0]))
    when 2
      args_opts.merge!(assign_value(args[0]))
      args_opts.merge!(assign_value(args[1]))
    when 3
      args_opts.merge!(assign_value(args[0]))
      args_opts.merge!(assign_value(args[1]))
      args_opts.merge!(assign_value(args[2]))
    end if arg_num <= 3
    options = {}
    if args_opts[:alldata].is_a?(Array)  &&
    args_opts[:alldata].size > 0 &&
    args_opts[:alldata][0] != '*'
      options[:order_by] = "#{args_opts[:alldata][0]} #{args_opts[:order].to_s}" if args_opts[:order]
    else
      options[:order_by] = "feature #{args_opts[:order].to_s}" if args_opts[:order]
    end
    options[:limit] = args_opts[:limit] if args_opts[:limit]
    if args_opts[:alldata].is_a?(Array)
      @house.filter({}, args_opts[:alldata], @home, options)
    else
      if args_opts[:alldata] == :all
        @house.filter({}, ['*'], @home, options)
      else
        @house.filter({}, ['feature', 'definition', 'mimetype'], @home, options)
      end
    end
  end

  def name
    if @family
      _self = find_self(@home, :family, true)
      if _self.size > 0
        _self = _self[0]
        @name = _self[:name]
      end
    else
      @name
    end
    @name
  end

  def related(relation)
    if @family
      name = normalize_name(relation)
      relation = @house.filter({:name => name}, [:name], @family, 1)
      if relation.size > 0
        result = @house.filter({:name => @name}, [:name], @family, 1)
        @house.update(result, {:related => name},  @family)
      else
        nil
      end
    else
      nil
    end
  end

  def relation
    if @family
      result = @house.filter({:name => @name}, [:related], @family, 1)
      if result[0][:related].empty?
        nil
      else
        result[0][:related]
      end
    else
      nil
    end
  end

  attr_reader :house_name
  attr_accessor :house, :home, :family, :status

end

if __FILE__ == $0
  def bootstrap
  end

  bootstrap

end
